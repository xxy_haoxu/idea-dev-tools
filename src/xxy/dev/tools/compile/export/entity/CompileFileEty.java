package xxy.dev.tools.compile.export.entity;

import com.intellij.openapi.vfs.VirtualFile;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class CompileFileEty {

    private static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public CompileFileEty(VirtualFile virtualFile) {
        this.virtualFile = virtualFile;
    }

    private String javaFilePath;

    private String classFilePath;

    private boolean compileOk;

    private String lastCompileTime;

    private VirtualFile virtualFile;

    private String javaFileName;

    private String classFileName;

    private String packagePath;

    public String getJavaFileName() {
        return javaFileName;
    }

    public void setJavaFileName(String javaFileName) {
        this.javaFileName = javaFileName;
    }

    public String getClassFileName() {
        return classFileName;
    }

    public void setClassFileName(String classFileName) {
        this.classFileName = classFileName;
    }

    public VirtualFile getVirtualFile() {
        return virtualFile;
    }

    public void setVirtualFile(VirtualFile virtualFile) {
        this.virtualFile = virtualFile;
    }

    public String getJavaFilePath() {
        return javaFilePath;
    }

    public void setJavaFilePath(String javaFilePath) {
        this.javaFilePath = javaFilePath;
    }

    public String getClassFilePath() {
        return classFilePath;
    }

    public void setClassFilePath(String classFilePath) {
        this.classFilePath = classFilePath;
    }

    public boolean isCompileOk() {
        return compileOk;
    }

    public void setCompileOk(boolean compileOk) {
        this.compileOk = compileOk;
    }

    public String getLastCompileTime() {
        return lastCompileTime;
    }

    public void setLastCompileTime(String lastCompileTime) {
        this.lastCompileTime = lastCompileTime;
    }

    public String getPackagePath() {
        return packagePath;
    }

    public void setPackagePath(String packagePath) {
        this.packagePath = packagePath;
    }

    public void readClassFileStatus() {
        if(StringUtils.isBlank(this.getClassFilePath())) {
            setCompileOk(false);
        }
        File classFile = new File(classFilePath);
        if(!classFile.exists()) {
            setCompileOk(false);
        }
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(classFile.lastModified());
        setLastCompileTime(DateTimeFormatter.ofPattern(TIME_FORMAT).format(LocalDateTime.now()));
    }
}
