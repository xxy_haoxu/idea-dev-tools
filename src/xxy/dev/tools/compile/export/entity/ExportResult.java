package xxy.dev.tools.compile.export.entity;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class ExportResult {

    public ExportResult() {

    }

    public ExportResult(String resultMessage, boolean success) {
        this.resultMessage = resultMessage;
        this.success = success;
    }

    public ExportResult(boolean cancel) {
        this.cancel = cancel;
    }

    private String resultMessage;

    private boolean success = true;

    private boolean cancel;

    private List<String> successFiles = Lists.newArrayList();

    private List<String> failedFiles = Lists.newArrayList();

    public void addSuccessFiles(String successFile) {
        if (StringUtils.isNotBlank(successFile))
            this.successFiles.add(successFile);
    }

    public void addFailedFiles(String failedFile) {
        if (StringUtils.isNotBlank(failedFile))
            this.failedFiles.add(failedFile);
    }

    public List<String> getSuccessFiles() {
        return successFiles;
    }

    public List<String> getFailedFile() {
        return failedFiles;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isCancel() {
        return cancel;
    }

    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }

    @Override
    public String toString() {
        StringBuilder message = new StringBuilder();
        message.append("Export Total:");
        message.append(this.successFiles.size());
        message.append(";");
        message.append("Success Files:");
        message.append(StringUtils.joinWith(",", this.successFiles));
        if(!this.success) {
            message.append(";");
            message.append("Failed Files:");
            message.append(StringUtils.joinWith(",", this.failedFiles));
        }
        return message.toString();
    }

    public static void main(String[] args) {
        List<String> fileNames = Lists.newArrayList();
        fileNames.add("a");
        fileNames.add("b");
        fileNames.add("c");
        String f = StringUtils.joinWith(",", fileNames);
        System.out.println(f);
    }
}
