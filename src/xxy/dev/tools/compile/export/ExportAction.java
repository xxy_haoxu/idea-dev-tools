package xxy.dev.tools.compile.export;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.ui.MessageUtil;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import xxy.dev.tools.compile.export.entity.CompileFileEty;
import xxy.dev.tools.compile.export.entity.ExportResult;
import xxy.dev.tools.compile.export.utils.ExportDirChooser;
import xxy.dev.tools.compile.export.utils.ExportUtil;
import xxy.dev.tools.compile.export.utils.VirtualFileToCompileFileTool;

import java.util.List;

public class ExportAction extends AnAction {
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        VirtualFile[] selectedFiles = e.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY);

        List<CompileFileEty> compileFileEties = VirtualFileToCompileFileTool.convertToCompileFile(selectedFiles, e.getProject());

        String exportDir = ExportDirChooser.chooseExportDir(e.getProject());

        ExportResult exportResult = ExportUtil.exportCompileFile(compileFileEties, exportDir);

        if(exportResult.isCancel()){
           return;
        }
        MessageUtil.showOkNoDialog(exportResult.isSuccess() ? "export success" : "export failed", exportResult.toString(), e.getProject(), "ok", "no", null);
    }
}
