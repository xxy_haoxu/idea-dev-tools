package xxy.dev.tools.compile.export.utils;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;
import xxy.dev.tools.compile.export.entity.CompileFileEty;

import java.io.File;
import java.util.List;

public class VirtualFileToCompileFileTool {
    private static final String JAVA_FILE_EXTENSION = "java";

    private static final String JAVA_PATH = "src" + File.separator + "main" + File.separator + "java";

    public static final String JAVA_EXTENSION = ".java";

    public static final String CLASS_EXTENSION = ".class";

    private static final String CLASSES_PATH = "build" + File.separator + "classes" + File.separator + "java" + File.separator + "main";

    public static List<CompileFileEty> convertToCompileFile(VirtualFile[] selectedFiles, @Nullable Project project) {
        if(ArrayUtils.isEmpty(selectedFiles)) {
            return Lists.newArrayList();
        }
        List<CompileFileEty> compileFileEties = Lists.newArrayList();
        for(VirtualFile virtualFile : selectedFiles) {
            CompileFileEty compileFileEty = convert(virtualFile);
            setPagePath(compileFileEty,project);
            compileFileEties.add(compileFileEty);
        }
        return compileFileEties;
    }

    private static void setPagePath(CompileFileEty compileFileEty, Project project) {
        String classFilePath = compileFileEty.getClassFilePath();
        String packagePath = classFilePath.substring(classFilePath.indexOf(CLASSES_PATH)).replace(CLASSES_PATH + File.separator, "");
        packagePath = packagePath.replace(File.separatorChar + compileFileEty.getClassFileName(), "");
        compileFileEty.setPackagePath(packagePath);
    }

    private static CompileFileEty convert(VirtualFile virtualFile) {
        if(null == virtualFile) {
            return null;
        }
        if(!StringUtils.equals(virtualFile.getExtension(), JAVA_FILE_EXTENSION)) {
            return null;
        }
        CompileFileEty compileFileEty = new CompileFileEty(virtualFile);
        String javaFilePath = virtualFile.getCanonicalPath();
        assert javaFilePath != null;
        File javaFile = new File(javaFilePath);
        String compiledFile = javaFile.getAbsolutePath().replace(JAVA_PATH, CLASSES_PATH).replace(JAVA_EXTENSION, CLASS_EXTENSION);
        compileFileEty.setJavaFilePath(javaFile.getAbsolutePath());
        compileFileEty.setClassFilePath(compiledFile);
        compileFileEty.setCompileOk(FileUtil.isCompiled(compiledFile));
        File javaPackageFile = new File(virtualFile.getParent().getPath());
        File classPackageFile = new File(javaPackageFile.getAbsolutePath().replace(JAVA_PATH,CLASSES_PATH));
        compileFileEty.setJavaFileName(javaFile.getAbsolutePath().replace( javaPackageFile.getAbsolutePath() + File.separator, ""));
        compileFileEty.setClassFileName(compiledFile.replace(classPackageFile.getAbsolutePath() + File.separator, ""));
        compileFileEty.readClassFileStatus();
        if(compileFileEty.isCompileOk()){
            return compileFileEty;
        }
        return null;
    }
}
