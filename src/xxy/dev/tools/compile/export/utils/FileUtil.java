package xxy.dev.tools.compile.export.utils;


import com.google.common.collect.Lists;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtil {
    public static boolean isCompiled(String compiledFile) {
        File file = new File(compiledFile);
        if(!file.exists()) {
            return false;
        }
        if(!compiledFile.endsWith(".class")) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        List<String> a = null;
        List<String> b = Lists.newArrayList("B");
        List<String> c = Stream.of(a,null).filter(Objects::nonNull).flatMap(Collection::stream).collect(Collectors.toList());
        System.out.println(c);
    }
}
