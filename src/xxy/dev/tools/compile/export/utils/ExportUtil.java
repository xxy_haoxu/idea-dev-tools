package xxy.dev.tools.compile.export.utils;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import xxy.dev.tools.compile.export.entity.CompileFileEty;
import xxy.dev.tools.compile.export.entity.ExportResult;

import java.io.*;
import java.io.FileReader;
import java.util.List;

public class ExportUtil {

    public static ExportResult exportCompileFile(List<CompileFileEty> compileFileEties, String exportDir) {
        if (CollectionUtils.isEmpty(compileFileEties)) {
            return new ExportResult("there are no file to export", true);
        }
        if(StringUtils.isBlank(exportDir)) {
            return new ExportResult(true);
        }
        ExportResult exportResult = new ExportResult();
        compileFileEties.forEach(file -> {
            if (!file.isCompileOk()) {
                exportResult.addFailedFiles(file.getJavaFileName());
            }
            String targetDir = String.format("%s" + File.separator + "%s", exportDir, file.getPackagePath());
            makeDir(targetDir);
            String targetClassFile = String.format("%s" + File.separator + "%s" + File.separator + "%s", exportDir, file.getPackagePath(), file.getClassFileName());
            copyFile(file.getClassFilePath(),targetClassFile);

            String targetJavaFile = String.format("%s" + File.separator + "%s" + File.separator + "%s", exportDir, file.getPackagePath(), file.getJavaFileName());
            boolean coppied = copyFile(file.getJavaFilePath(), targetJavaFile);
            if (!coppied) {
                exportResult.addFailedFiles(file.getJavaFileName());
            }
            exportResult.addSuccessFiles(file.getJavaFileName());
        });
        if(CollectionUtils.isNotEmpty(exportResult.getFailedFile())) {
            exportResult.setSuccess(false);
        }
        return exportResult;
    }

    private static void makeDir(String targetDir) {
        File dir = new File(targetDir);
        if(!dir.exists()) {
            boolean mkdirs = dir.mkdirs();
            assert mkdirs;
        }
    }

    private static boolean copyFile(String srcFile, String targetFile) {
        try {
            File target = new File(targetFile);
            if(!target.exists()) {
                target.createNewFile();
            }
            FileUtils.copyFile(new File(srcFile), new File(targetFile));
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        List<String> fileNames = Lists.newArrayList();
        fileNames.add("a");
        fileNames.add("b");
        fileNames.add("c");
        String f = StringUtils.joinWith(",", fileNames);
        System.out.println(f);
    }
}
