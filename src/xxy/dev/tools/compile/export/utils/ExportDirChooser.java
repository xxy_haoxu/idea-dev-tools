package xxy.dev.tools.compile.export.utils;

import com.intellij.ide.actions.OpenProjectFileChooserDescriptor;
import com.intellij.openapi.fileChooser.FileChooser;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import org.apache.commons.collections.CollectionUtils;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public class ExportDirChooser {
    public static String chooseExportDir(@Nullable Project project) {
        assert project != null;
        VirtualFile toSelect = VfsUtil.findFileByIoFile(new File(Objects.requireNonNull(project.getBasePath())),false);
        AtomicReference<String> exportDir = new AtomicReference<String>();
        FileChooser.chooseFiles(new ExportDirChooserDescriptor(true), project, toSelect != null ? toSelect : VfsUtil.getUserHomeDir(), files -> {
            if(CollectionUtils.size(files) == 1){
                exportDir.set(((VirtualFile)CollectionUtils.get(files,0)).getCanonicalPath());
            }
        });
        return exportDir.get();
    }

    private static class  ExportDirChooserDescriptor extends OpenProjectFileChooserDescriptor{

        public ExportDirChooserDescriptor(boolean chooseFiles) {
            super(chooseFiles);
            setTitle("title.export.dir");
        }
    }
}
